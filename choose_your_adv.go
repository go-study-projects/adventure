package choose_your_adv

import (
	"embed"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"text/template"
)

func init() {
	tmpl = template.Must(template.ParseFS(fs, templateFilename))
}

var tmpl *template.Template

//go:embed template.html
var defaultHandlerTmpl string

//go:embed sample_story.json
//go:embed template.html
var fs embed.FS

const defaultStoryFilename = "sample_story.json"
const templateFilename = "template.html"

type Chapter struct {
	Title      string   `json:"title"`
	Paragraphs []string `json:"story"`
	Options    []struct {
		Text    string `json:"text"`
		Chapter string `json:"arc"`
	} `json:"options"`
}

type Story map[string]Chapter

func NewHandler(s Story, opts ...HandlerOption) http.Handler {
	h := handler{s, tmpl}
	for _, opt := range opts {
		opt(&h)
	}
	return h
}

type handler struct {
	story    Story
	template *template.Template
}

type HandlerOption func(h *handler)

func WithTemplate(t *template.Template) HandlerOption {
	return func(h *handler) {
		if t != nil {
			h.template = t
		}
	}
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	if path == "" || path == "/" {
		path = "/intro"
	}

	path = path[1:]

	if _, exists := h.story[path]; exists {
		if err := h.template.Execute(w, h.story[path]); err != nil {
			log.Printf("%v", err)
			http.Error(w, "Rekt", http.StatusInternalServerError)
		}
		return
	}
	http.Error(w, "Chapter not found.", http.StatusNotFound)
}

func GetStory(filename string) (Story, error) {
	if filename == "" {
		filename = defaultStoryFilename
	}
	content, err := fs.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	story := make(Story)
	if err := json.Unmarshal(content, &story); err != nil {
		return nil, err
	}

	if _, exits := story["intro"]; !exits {
		msg := "\n\n~~~ In every tale, a commencement must be found, yet thine doth lack. ~~~\n\n" +
			"TLDR: story file does not have an 'intro' chapter\n\n"
		return nil, errors.New(msg)
	}

	return story, nil
}
