package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	adv "gitlab.com/go-study-projects/adventure"
)

func main() {
	filename := flag.String("file", "", "json file with story")
	port := flag.Int("port", 3000, "port to start the server")
	flag.Parse()

	story, err := adv.GetStory(*filename)
	if err != nil {
		log.Fatal("Error parsing file:", err)
	}

	handler := adv.NewHandler(story)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), handler))
}
